package SMS::Send::Driver::Infobip;

use strict;
use warnings;

use JSON::XS;
use HTTP::Tiny;
use DateTime;
use Encode qw(encode decode);
use Carp;
use URI;
use utf8;

use base 'SMS::Send::Driver';

our $VERSION = '0.01';

use constant {
    DEFAULT_SERVICE_HOST => "api.infobip.com",
    DEFAULT_RESOURCE_PATH => "/sms/2/text/single"
};



sub new {
    my ($class, %args) = @_;

    my $uri = URI->new();
    $uri->scheme('https');
    $uri->host(defined($args{_host}) ? $args{_host} : DEFAULT_SERVICE_HOST);

    my $default_headers = { verify_SSL => 1};
    
    if (defined($args{_key})) {
	$default_headers->{authorization} = "App $args{_key}";
    } else {
	$uri->userinfo($args{_login} . ':' . $args{_password}) unless defined($args{_key});
    }

    $uri->path(DEFAULT_RESOURCE_PATH);

    my $http = HTTP::Tiny->new(default_headers => $default_headers);

    die "No valid url!" unless defined($uri);

    my $self = bless {
	uri => $uri,
	login => $args{_login},
	password => $args{_password},
	http => $http
    };
}

sub _prepare {
    my ($self, %args) = @_;

    my $to;
    eval { $to = normalize_phone_number( $args{to} ); };
    if ($@) {
	return 0;
    }

    my $ctx = {
	to => $to,
	text => $args{text}
    };

    my $uri = $self->{uri}->clone();

    return ('POST', $uri->as_string, {
	content => encode_json($ctx),
	headers => {
	    "content-type" => "application/json",
	    "accept" => "application/json"
	}
    });
    
}

sub send_sms {
    my $self = shift;

    my $resp = $self->{http}->request($self->_prepare(@_));

    return $resp->{success};
}


sub normalize_phone_number {
    my $nr = shift;

    $_ = $nr;
    s/(^[^+0-9])|((?<=.)[^0-9])//g;
    s/^((\+\+)|(\+?00))/+/;
    s/^0/+46/;

    croak "Invalid phone number '$nr' ('$_')" unless /^\+\d*$/;

    return $_;
}

sub is_swedish_mobile_phone {
    my $nr = shift;
    return $nr =~ /^\+467(([02369])|(4[123457])|(10))/;
}
    
1;
__END__

=head1 NAME

SMS::Send::Driver::Infobip - SMS::Send::Driver for Infobips API

=head1 SYNOPSIS

  use SMS::Send::Driver::Infobip;

  my $sender = SMS::Send->new(
    'Driver::Infobip',
    _company => 'company'
    _login    => 'myname',
    _password => 'mypassword',
    _sender => 'optional sender string',
  );

  $sender->send_sms(to => '+46XXXXXXXXXX',
                    text => 'text message');

=head1 DESCRIPTION

=head1 AUTHOR

Andreas Jonsson, E<lt>andreas.jonsson@kreablo.se<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2018 by Andreas Jonsson

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.26.0 or,
at your option, any later version of Perl 5 you may have available.


=cut
