# Before 'make install' is performed this script should be runnable with
# 'make test'. After 'make install' it should work as 'perl SMS-Send-Driver-Infobip.t'

#########################

# change 'tests => 1' to 'tests => last_test_to_print';

use utf8;
use strict;
use warnings;

use Test::More tests => 5;
BEGIN { use_ok('SMS::Send::Driver::Infobip') };

#########################

# Insert your test code below, the Test::More module is use()ed here so read
# its man page ( perldoc Test::More ) for help writing this test script.

use SMS::Send;
use DateTime;
use Encode qw(encode decode);
use Encode::Encoder;
use JSON::XS;

binmode STDERR;

sub test_prepare_http_data {

  my $sender = SMS::Send->new(
      'Driver::Infobip',
      _login    => 'myname',
      _password => 'mypassword',
      _sender => 'sender'
      );

  my $sendDt = DateTime->now;

  my @args = $sender->{OBJECT}->_prepare(to => '070 123456', text => 'hello');

  is($args[0], 'POST', 'Method');
  is($args[1], 'https://myname:mypassword@api.infobip.com/sms/2/text/single', 'url');

  my $ctx = decode_json($args[2]->{content});

  is($ctx->{to}, "+4670123456");
  is($ctx->{text}, "hello");

  return 0;
}

sub test_service {
    my $sender = SMS::Send->new(
      'Driver::Infobip',
      _login    => '',
      _password => '',
	);

    my $sent = $sender->send_sms(text => 'Hallå', to => '');

    is($sent, 1, 'message was sent');
}

test_prepare_http_data();
# test_service();

1;
